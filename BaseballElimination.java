import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.HashMap;

public class BaseballElimination {

    private final HashMap<String, Integer> teamNameIdMap;
    private final HashMap<Integer, String> teamIdNameMap;
    private int[] wins;
    private int[] losses;
    private int[] remainingGames;
    private int[][] remainingGamesVersus;

    private boolean[] isEliminated;
    private HashMap<Integer, ArrayList<String>> certificatesOfElimination;
    private final int n;
    private static final double INFINITY = Double.POSITIVE_INFINITY;
    private static final int NUM_INPUT_FIELDS = 4;

    // losses array never used?
    public BaseballElimination(String filename) {
        In in = new In(filename);
        this.n = Integer.parseInt(in.readLine());
        this.teamNameIdMap = new HashMap<>();
        this.teamIdNameMap = new HashMap<>();
        this.wins = new int[n];
        this.losses = new int[n];
        this.remainingGames = new int[n];
        this.remainingGamesVersus = new int[n][n];
        loadData(in);

        this.isEliminated = new boolean[n];
        this.certificatesOfElimination = new HashMap<>();
        for (int x = 0; x < n; x++) {
            checkElimination(x);
        }
    }                    // create a baseball division from given filename in format specified below

    private void loadData(In in) {
        int teamId = 0;
        while (!in.isEmpty()) {
            String line = in.readLine();
            String[] spaceSeparated = line.split(" ");
            String[] lineElements = new String[NUM_INPUT_FIELDS + this.n];
            int i = 0;
            for (int j = 0; j < spaceSeparated.length; j++) {
                if (!spaceSeparated[j].equals("")) {
                    lineElements[i++] = spaceSeparated[j];
                }
            }
            teamNameIdMap.put(lineElements[0], teamId);
            teamIdNameMap.put(teamId, lineElements[0]);
            wins[teamId] = Integer.parseInt(lineElements[1]);
            losses[teamId] = Integer.parseInt(lineElements[2]);
            remainingGames[teamId] = Integer.parseInt(lineElements[3]);

            for (int otherId = 0; otherId < n; otherId++) {
                remainingGamesVersus[teamId][otherId] = Integer.parseInt(
                        lineElements[NUM_INPUT_FIELDS + otherId]);
            }
            teamId++;
        }
    }

    private void checkElimination(int x) {
        int maxWins = -1;
        int maxIndex = -1;
        for (int i = 0; i < wins.length; i++) {
            if (wins[i] > maxWins) {
                maxWins = wins[i];
                maxIndex = i;
            }
        }
        if (wins[x] + remainingGames[x] < maxWins) {
            trivialElimination(x, maxIndex);
        }
        else checkMathematicalElimination(x);
    }

    private void trivialElimination(int x, int maxIndex) {
        this.isEliminated[x] = true;
        ArrayList<String> certificate = new ArrayList<>();
        certificate.add(teamIdNameMap.get(maxIndex));
        this.certificatesOfElimination.put(x, certificate);
    }

    private void checkMathematicalElimination(int x) {
        int numVerticesTotal = 1 + (this.n - 1) * (this.n - 2) / 2 + this.n;
        int numGameVertices = (this.n - 1) * (this.n - 2) / 2;
        FlowNetwork flowNetwork = new FlowNetwork(numVerticesTotal);
        int src = 0, currGameVertex = 1, sink = numVerticesTotal - 1;
        ArrayList<FlowEdge> edgesFromSrc = new ArrayList<>();
        for (int teamA = 0; teamA < this.n - 1; teamA++) {
            if (teamA == x) continue;
            for (int teamB = teamA + 1; teamB < this.n; teamB++) {
                if (teamB == x) continue;
                FlowEdge srcToGame = new FlowEdge(src, currGameVertex,
                                                  remainingGamesVersus[teamA][teamB]);
                flowNetwork.addEdge(srcToGame);
                edgesFromSrc.add(srcToGame);

                int teamAVertex = idToVertex(teamA, x, numGameVertices);
                int teamBVertex = idToVertex(teamB, x, numGameVertices);
                flowNetwork.addEdge(new FlowEdge(currGameVertex, teamAVertex, INFINITY));
                flowNetwork.addEdge(new FlowEdge(currGameVertex, teamBVertex, INFINITY));

                currGameVertex++;
            }
        }
        for (int teamVertex = numGameVertices + 1; teamVertex < numVerticesTotal - 1;
             teamVertex++) {
            int teamId = vertextToId(teamVertex, x, numGameVertices);
            double allowedWins = Math.max(0, wins[x] + remainingGames[x] - wins[teamId]);
            flowNetwork.addEdge(new FlowEdge(teamVertex, sink, allowedWins));
        }

        FordFulkerson ff = new FordFulkerson(flowNetwork, src,
                                             sink);
        for (FlowEdge e : edgesFromSrc
        ) {
            if (e.residualCapacityTo(e.other(src)) != 0) {
                mathematicalElimination(x, numVerticesTotal, numGameVertices, ff);
                break;
            }
        }
    }

    private void mathematicalElimination(int x, int numVerticesTotal, int numGameVertices,
                                         FordFulkerson ff) {
        this.isEliminated[x] = true;
        ArrayList<String> certificate = new ArrayList<>();
        for (int teamVertex = numGameVertices + 1; teamVertex < numVerticesTotal - 1;
             teamVertex++) {

            if (ff.inCut(teamVertex)) {
                certificate.add(
                        teamIdNameMap.get(vertextToId(teamVertex, x, numGameVertices)));
            }
        }
        this.certificatesOfElimination.put(x, certificate);
    }

    private int idToVertex(int teamId, int x, int numGameVertices) {
        if (teamId < x) return teamId + numGameVertices + 1;
        else return teamId + numGameVertices;
    }

    private int vertextToId(int vertex, int x, int numGameVertices) {
        int tmp = vertex - numGameVertices;
        return tmp <= x ? tmp - 1 : tmp;
    }


    public int numberOfTeams() {
        return this.n;
    }                      // number of teams

    public Iterable<String> teams() {
        return teamIdNameMap.values();
    }                          // all teams

    public int wins(String team) {
        if (!teamNameIdMap.containsKey(team)) throw new IllegalArgumentException();
        return wins[teamNameIdMap.get(team)];
    }                   // number of wins for given team

    public int losses(String team) {
        if (!teamNameIdMap.containsKey(team)) throw new IllegalArgumentException();
        return losses[teamNameIdMap.get(team)];
    }                 // number of losses for given team

    public int remaining(String team) {
        if (!teamNameIdMap.containsKey(team)) throw new IllegalArgumentException();
        return remainingGames[teamNameIdMap.get(team)];
    }                 // number of remaining games for given team

    public int against(String team1, String team2) {
        if (!teamNameIdMap.containsKey(team1) || !teamNameIdMap.containsKey(team2))
            throw new IllegalArgumentException();
        int team1Id = teamNameIdMap.get(team1);
        int team2Id = teamNameIdMap.get(team2);
        return remainingGamesVersus[team1Id][team2Id];
    }     // number of remaining games between team1 and team2

    public boolean isEliminated(String team) {
        if (!teamNameIdMap.containsKey(team)) throw new IllegalArgumentException();
        return this.isEliminated[teamNameIdMap.get(team)];
    }                // is given team eliminated?

    public Iterable<String> certificateOfElimination(String team) {
        if (!teamNameIdMap.containsKey(team)) throw new IllegalArgumentException();
        return this.certificatesOfElimination.get(teamNameIdMap.get(team));
    }    // subset R of teams that eliminates given team; null if not eliminated

    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            }
            else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }
}