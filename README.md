# Algorithms II Assignment 3: Baseball Elimination
Solution for assignment 3 of the Princeton University Algorithms II course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/baseball/specification.php).

The goal of this assignment was to use the notion of a flow network and the Ford-Fulkerson algorithm for finding minimum cuts in such network to calculate which teams in a division of American baseball are "mathematically eliminated", ie. unable to finish first in their group due to the necessary distribution of wins among the other teams in the group.

## Results  

Correctness:  23/23 tests passed

Memory:       4/4 tests passed

Timing:       1/1 tests passed

<b>Aggregate score:</b> 100.00%

## How to use
BaseballElimination.java accepts a .txt input file in the format specified in the assignment description above, and outputs for each team whether they are mathematically eliminated in the group according to the current standings.
<b>NOTE:</b> Depends on the algs4 library used in the course.
